#!/usr/bin/env python3
"""This script contains code sections which you will have to match using Semgrep"""
import os
import requests
import subprocess

# ***************************FIRST SECTION***********************
users = {1: "Mike", 2: "Jake", 3: "Bob", 4: "Jenny"}
print("Username: " + users[1])  # Pattern 1 match
print("Username: " + users[2])  # Pattern 1 match
print("Username: " + users[3])  # Pattern 1 match
print("Username: " + users[4])  # Pattern 1 match


# ***************************SECOND SECTION***********************

def addition(num1, num2):
    return num1+num2


def get_current_directory():
    os.system("pwd")  # Pattern 2 match


def subtraction(num3, num4):
    num5 = num4 - num3
    os.system("echo "+num5)  # Pattern 2 match


def hack_system():
    print("System hacked! " + os.uname())  # Pattern 2 match

# ***************************THIRD SECTION***********************


requests.get("example.com", timeout=30, verify=True)  # Pattern 3 match
requests.get("example.com", verify=True, timeout=10)  # Pattern 3 match

# but do not match these
requests.get("example.com", timeout=10)
requests.get("example.com", verify=True)
requests.get("example.com", verify=False, timeout=10)
requests.get("example.com", secure=True, timeout=10)


# ***************************FOURTH SECTION***********************

def compare(val1, val2):
    if val1 == "Value one":
        print("Pattern 4 MATCH")  # Pattern 4 match
    if "Value two" == val2:
        print("Another Pattern 4 MATCH")  # Pattern 4 match


# ***************************FIFTH SECTION***********************
nonstring = 1
subprocess.call("ping 8.8.8.8")  

dir = "/home"
subprocess.call("cd " + dir)  

subprocess.call(dir, shell=True)  

subprocess.call(nonstring)  # Pattern 5 match


# ***************************SIXTH SECTION***********************

def func1(param11, param2, param3):
    var1 = 1+1
    print("This should be a match")  # Pattern 6 match
    return(var1 - param11)


def func2(param11, param21, param3):
    print("This should not")


def func3(param11, param2, param3):
    var2 = param2 + param3
    var3 = var2 - param11
    print("This should also be")  # Pattern 6 match
